<!DOCTYPE html>
<html>

</script>

</head>

<!-- Page body -->

<!-- The Index Page -->

<head>

<!-- Meta Info -->
<meta charset="UTF-8">
<title>Alex Horlock</title>
<meta name="description" content= "University of Southampton Aerospace engineering graduate working in AI and pursuing chartered engineer status">
<meta name="author" content="Alex Horlock">
<meta name="viewport" content="width=700">
 
<!-- Link Required CSS Stylesheets and Fonts -->

<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Josefin+Slab">
<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:400,900"  >
<link href="../css/homestyle.css" type="text/css" rel="stylesheet">

<!-- Link jquery -->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<!-- Page animations/ interactions -->

<script>

$(document).ready(function(){
   $("#menu").hide();
      $("#menuicon").click(function(){
        $("#menu").toggle("fast");
    });
});
</script>

<body>
	<!--<div id="page-wrap">
           <p>Ho hum</p>
	</div>-->

<div id="header">

	<div id="menuicon">
	 	<img src="../images/menu_icon.png" alt ="Menu" height="50" width="50">
	</div>
	
	<div id="title">
		<span class="bold">Horlock's</span>|Homepage 
	</div>

	<!-- Social Network Links -->

	<div id="socialIconBar">
		<div class="socialicon" >
		<a href="http://www.facebook.com/alex.horlock.5">
			<img src="../images/facebook_icon.png" alt="" height="40" width="40">
		</a>
		</div>

		<div class="socialicon" >
		<a href="http://www.linkedin.com/pub/alex-horlock/6a/92/52">
			<img src="../images/linkedin_icon.png" alt="" height="55" width="55" >
		</a>
		</div>
	
		<div class="socialicon" >

		<a href="http://www.twitter.com/AlexHorlock1">
			<img src="../images/twitter_icon.png" alt="" height="50" width="50">
		</a>
		</div>

		<div class="socialicon" >

		<a href="https://mail.google.com/mail/#inbox">
			<img src="../images/gmail_icon.png" alt="" height="50" width="50">
		</a>
		</div>

	</div>

</div>

<ul id="menu">
	<li> <a href="http://www.horlockspage.com">Home</a></li>
	<li> <a href="http://www.horlockspage.com/about.php">Overview</a>
	<li> <a href="http://www.horlockspage.com/projects.php">Projects</a></li> 
	<li> <a href="http://www.horlockspage.com/sport.php">Sport</a></li>
	<li> <a href="http://www.horlockspage.com/contact.php">Contact</a></li>
	<li> <a href="http://www.horlockspage.com/x.php">Private</a><li>
</ul>




<!--PHP Hit counter-->

<div id="viewcount"><?php include 'php/counter2.php';?></div>

<p id="quote" > <i> "Neither success nor failure is ever final" </i> <br> - Ron Dennis</p>

</body>

<!-- ##### Notes #####

The Z-index controls which elements appear on the top layer.
PHP must be inside the body of the text
-->
</html>
