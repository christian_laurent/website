<?php header('Content-Type: text/html; charset=utf-8')
if( isset($_POST['q']) ){
	$q = nl2br($_POST['q']);
	$a = $_POST['a'];
	$p = nl2br($_POST['p']);
	$data = $q . ',' . $a . ',' . $p . "\n";
	$return = file_put_contents('./invictus.csv', $data, FILE_APPEND | LOCK_EX);
    if($return === false) {
        die('There was an error adding this quote :`(');
    }
    else {
        echo "Quote added! $return bytes written to file";
    }
}
else {
   die('no post data to process');
}

?>